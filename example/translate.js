/*
 * # locale library usage examples
 *
 * ## locale.js: i18n for Node.js and browser
 * 
 * @author Dmitry A. Chleck <dmitrychleck@gmail.com>
 * @version 0.0.2
 */

// Import and init the library
var locale = require('..');
locale.init({ 'lang': 'en', 'path': './' });

// Pull i18n functions to the local context:
var __   = locale.__;

w('# LOCALE LIBRARY DEMO:');

// We set English translation in init() function
w('## English:');
demo(); // <- demo of usage of translation functions - see below

// Switch to Russian:
locale.lang = 'ru';

w('## Russian:');
demo();

// Let's create 2 i18n object, English and Russian:
var i18n_en = new locale.i18n({ 'lang': 'en' });
var i18n_ru = new locale.i18n({ 'lang': 'ru' });

// Pull i18n functions from English object:
__   = i18n_en.__;

// Remember, library's language still set to Russian, but we see English messages. This functions uses i18n_en object context:
w('## English again:');
demo();

// Now lets pull i18n functions from Russian object:
__   = i18n_ru.__;

// And we see Russian text again:
w('## Russian again:');
demo();

// THE END

// !!! Usage of translation functions
function demo() {
  // Translate phrase
  w(__('Hello!'));
  // Comment for translator
  w(__('Hello!// This is comment.'));
  // Phrase with id 
  w(__('Hello!//another_hello'));
  // Phrase with id and comment
  w(__('Hello!//another_hello2 Please translate this another way.'));
  // Phrase with // but not comment
  w(__('There is // in this phrase but it is not comment.//'));
  // This phrase will not be translated - missing in translation.
  w(__('Hello?'));
  // Escapes for placeholders
  w(__('This is %% percent symbol. This is placeholder with %s((name).', 'ignored '));
  // Placeholders with additional arguments
  w(__('My %s(1) is faster then your %s(2)!', 'SSD', 'HDD'));
  // Placeholders with array
  w(__('My %s(1) is faster then your %s(2)!', [ 'Kawasaki', 'Segway' ]));
  // Placeholders with object
  w(__('My %s(1) is faster then your %s(2)!', { 1: 'Core i7', 2: '486DX' }));
  // Both names and order
  w(__('Let\'s count in English: %s, %s, %s(4) and %s.', 'one', 'two', 'four', 'three'));
  // Plural forms
  w(__(['Inbox: %n unreaded message.', 'Inbox: %n unreaded messages.'], 1));
  w(__(['Inbox: %n unreaded message.', 'Inbox: %n unreaded messages.'], 12));
  w(__(['Inbox: %n unreaded message.', 'Inbox: %n unreaded messages.'], 22));
  // All-in-one
  w(__([
    '%n developer from our team uses %s(1) with %s(2).',
    '%n developers from our team uses %s(1) with %s(2).'
    ], 1, 'C', 'vim'
  ));
  w(__([
    '%n developer from our team uses %s(1) with %s(2).',
    '%n developers from our team uses %s(1) with %s(2).'
    ], 3, [ 'Python', 'PyCharm' ]
  ));
  w(__([
    '%n developer from our team uses %s(1) with %s(2).',
    '%n developers from our team uses %s(1) with %s(2).'
    ], 7, { '1': 'Node.js', '2': 'Sublime Text 2' }
  ));
  // No args - empty string
  w(__());
}

// Some support functions

// The short way message
function w(s) { console.log(s); }
