/*
 * # locale library usage examples: remote translation
 *
 * ## locale.js: i18n for Node.js and browser
 * 
 * @author Dmitry A. Chleck <dmitrychleck@gmail.com>
 * @version 0.0.2
 */

http = require('http');

// Import and init the library
var locale = require('..');
locale.init({ 'lang': 'en', 'path': './' });
locale.remote = true;

var __ = locale.__;

// Start HTTP server on 127.0.0.1:8080
http.createServer(function (req, res) {
  // Make JSON with i18n fields
  var json = {
    'title': __('Hello!'),
    'msg': __(['The following error occurred during processing:', 'The following errors occurred during processing:'], 1),
    'error': [ __('Hello!'), __('Email %s is invalid. Please enter valid email.', '%#$@gmail.com') ],
    'session': 'jsahfkjsdfsdhiudfshiuh'
  };
  // and submit it to HTTP client
  res.writeHead(200, {'Content-Type': 'application/json'});
  res.end(JSON.stringify(json, null, '  '));
}).listen(8080, '127.0.0.1');

// Get JSON from HTTP server
http.get({ host:'127.0.0.1', port:8080, path:'/', agent:false }, function (res)
{
  var data = '';
  res.setEncoding('utf8');
  res.on('data', function (chunk) {
    data += chunk;
  });
  res.on('end', function(){
    var json;
    // Translate to Russian
    json = JSON.parse(data);
    locale.lang = 'ru';
    locale.__JSON(json);
    console.log('\nRussian translation:\n', JSON.stringify(json, null, '  '));
    // Translate to English
    json = JSON.parse(data);
    locale.lang = 'en';
    locale.__JSON(json);
    console.log('\nEnglish translation:\n', JSON.stringify(json, null, '  '));
    // Exit
    process.exit(0);
  });
});
