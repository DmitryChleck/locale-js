var locale = require('locale-js');

__();

__('');

__('1');

var some_space = 123;

__("Hello!// This is comment")

__("Hello!//testid This is comment with id")

__("Hello!//id_without_comment")

__("This is// not comment.//")

//__("Bye-bye!")

some_space += 321; // __('Ignored')

var more;

/* Code below will be ignored

__('Ignored again');

*/

__('Goodbye...');

console.log('Test file for locale-js parser.');

__('In phrase \' or ) or __('); var a = ');'

var wow = '__('; var = 'Bad string )';

__(['%n byte', '%n bytes'], 4);

__(['%n error', '%n errors'], 4);

__("Long text.\nMultiple lines.\nMore and more.")
