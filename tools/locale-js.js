var fs = require('fs')
  , path = require('path')
  , i = require('util').inspect;

var w = console.log;

var p = new parser();

var action = process.argv[2] || '';
switch(action) {
  case 'tr':      tr(); break;
  case 'json':  json(); break;
  case 'clean': clean(); break;
  default:      help();
}

// Actions

function tr() {
  p.js2tr();
}

function json() {
  p.tr2json(2, 3, 'ru');
}

function clean() {
  
}

function help() {
  console.log('\nUsage: locale-js <action>\n\n'
  + '$ locale-js tr     Create or update *.tr files for the current project.\n'
  + '$ locale-js json   Convert *.tr to *.json.\n'
  + '$ locale-js clean  Remove *.tr from project.\n'
  );
}

function parser() {

  // tr2json
  this.tr2json = function(n, m, file) {
    var i = 0;
    var plural = 0;
    var key = '';
    var tr = [];
    var json = {};
    var s = fs.readFileSync(fileChangeExt(file, '.tr'), 'utf8');

    s.split('\n').forEach(function(l) {
      i++;
      var c = l[0];
      var l = l.substr(2);
      switch(c) {
        // Base
        case '?':
          if(tr.length) trError('"?" after "!".');
          if(key) plural++;
          if(!key) key = l;
          break;
        // Translation
        case '!':
          tr.push(l);
          break;
        // Multiline
        case '\\':
          if(!tr.length) {
            // Uppend to key (first base form)
            if(!plural) key += '\n' + l;
          } else {
            // Uppend to translation (any form)
            tr[tr.length-1] += '\n' + l;
          }
          break;
        // Comment - ignore
        case '#': return;
        // Next phrase
        default:
          if(plural && plural+1 != n) trError('found ' + (plural+1) + ' "?" strings (must be 1 or ' + n + ').');
          if(plural && tr.length != m) trError('found ' + tr.length + ' "!" strings (must be ' + m + ').');
          json[key] = plural ? tr : tr[0];
          plural = 0;
          key = '';
          tr = [];
      }
    });
    fs.writeFileSync(fileChangeExt(file, '.json'), JSON.stringify(json, null, '  '), 'utf8');
    w(JSON.stringify(json, null, '  '));

    // Support functions
    function trError(msg) {
      console.log('Error at line ' + i + ': ' + msg);
      process.exit();
    }
  }

  this.js2tr = function() {

    // # Parse .js files

    var nBase = 2;
    var nTr = 3;
    var ofile = 'ru.tr';

    var tr = require(path.resolve('./tr.json'));

    var f = '';
    for(var k in tr.files) {
      f += fs.readFileSync(tr.files[k], 'utf8');
    }
    // Set position to 0
    var i = 0;
    // Result
    res = {};
    while(i < f.length) {
      switch(f[i]) {
        // Skip 'strings'
        case '\'': str('\''); break;
        // Skip "strings"
        case '"': str('"'); break;
        // Skip // and /* */ comments
        case '/': if(f[i+1] == '/') comment1(); else if(f[i+1] == '*') comment2(); break;
      }
      // Parse __()
      if(f.substr(i, 3) == '__(') __();
      i++;
    }

    w(JSON.stringify(res, null, ' '));

    // # Write .tr file

    // Read old .json
    var old = fs.readFileSync(fileChangeExt(ofile, '.json'), 'utf8');
    // Load old .json translation
    old = JSON.parse(old);
    var out = [];
    for(var key in res) {
      w('+++', key, old[key]);
      res[key].phrase.forEach(function(a) {
        // Push comment if present
        if(a[2]) out.push('# ' + a[2].replace(/\n/g, '\n\\ '));
        // Push phrase + id
        out.push('? ' + (a[0] + a[1]).replace(/\n/g, '\n\\ '));
      });
      for(var i = 0; i < (res[key].plural ? nTr : 1); i++) {
        var tmp = old[key];
        if(tmp && res[key].plural) tmp = tmp[i];
        tmp = tmp || '';
        out.push('! ' + tmp.replace(/\n/g, '\n\\ '));
      }
      out.push('');
    }

    // Join output data
    out = out.join('\n');
    // Rewrite .tr file
    fs.writeFileSync(fileChangeExt(ofile, '.tr'), out, 'utf8');
    w('\nFile: ' + fileChangeExt(ofile, '.tr') + '\n\n' + out);

    // # Support functions

    // Parse __()
    function __() {
      plural = false;
      row = [];
      i += 3;
      while(i < f.length) {
        // Symbols allowed after __(
        if(')\'"[\n\r\t '.indexOf(f[i]) < 0) error('"' + f[i] + '" after "_(("');
        // Actions
        switch(f[i]) {
          // __() without args
          case ')':
            return;
          // __([ plural form
          case '[':
            if(plural) error('second "[" between "__(" and args'); plural = true;
            break;
          // __(' first argument
          case '\'':
          case '"':
            key();
            // Skip if all keys are empty
            if(!row.join('')) return;
            // Error if incorrect number of plural forms
            if(plural && row.length != nBase) error('You need ' + nBase + ' plural forms for your base language.');
            var phrase = []
              , id, comment, pos;
            // Split string to phrase, id and comment
            row.forEach(function(s) {
              id = '';
              comment = '';
              pos = s.lastIndexOf('//');
              if(pos >= 0) {
                id = s.substring(pos);
                s = s.substring(0, pos);
                pos = id.indexOf(' ');
                if(pos >= 0) {
                  comment = id.substring(pos + 1);
                  id = id.substring(0, pos);
                  if(id.length < 3) id = '';
                }
              }
              phrase.push([ s, id, comment ]);
            });
            res[phrase[0][0] + phrase[0][1]] = {
              'plural': plural,
              'phrase': phrase
            };
            return;
        }
        i++;
      }
    }

    // Parse argument value from current position to end of string
    function key() {
      // Save string quote and skip it
      var c = f[i++];
      var from = i;
      while(i < f.length) {
        if(f[i] == c && f[i-1] != '\\') {
          // Save arg value
          row.push(eval('\'' + f.substring(from, i) + '\''));
          // Get next key for plural
          if(plural) nextKey();
          return;
        }
        i++;
      }
    }

    // Skip to next key
    function nextKey() {
      // Skip closing ' or " from previous arg
      i++;
      while(i < f.length) {
        // Symbols allowed after __(['phrase1'
        if(']\'",\n\r\t '.indexOf(f[i]) < 0) error('"' + f[i] + '" between args');
        // Actions
        switch(f[i]) {
          // No more args
          case ']': return;
          // Next arg
          case '\'': key(); return;
        }
        i++;
      }
    }

    // Skip from current position to end of string
    function str(c) {
      while(i < f.length) {
        i++;
        if(f[i] == c && f[i-1] != '\\') return;
      }
    }
    
    // Skip from current position to end of line
    function comment1() {
      while(i < f.length) {
        i++;
        if(f[i] == '\n') return;
      }
    }
    
    // Skip from current position to end of multiline comment
    function comment2() {
      while(i < f.length) {
        i++;
        if(f[i] == '/' && f[i-1] == '*') return;
      }
    }

    // Print error and abort processing
    function error(msg) {
      var l = 1
        , c = 1;
      for(var p = 0; p < i; p++) {
        if(f[p] == '\n') {
          l++;
          c = 0;
        }
        c++;
      }
      console.log('Error at ' + l + ', ' + c + ':\n' + msg + '\n');
      process.exit();
    }

  } // End of js2tr

} // End of parser

function fileChangeExt(file, ext) {
  return file.substring(0, file.length - path.extname(file).length) + ext;
}
