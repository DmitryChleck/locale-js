/*
 * # locale library test suite
 *
 * ## locale.js: i18n for Node.js and browser
 * 
 * @author Dmitry A. Chleck <dmitrychleck@gmail.com>
 * @version 0.0.2
 */

locale = require('..');

var i18n
  , __  = locale.__;

describe('Test suite for locale library', function() {

  describe('Initialisation:', function() {

    it('init()', function() {
      locale.init({ 'path': './test/locale' });
    })

  })

  describe('Pass through (base language):', function() {

    it('Simple', function() {
      __('Hello!').should.equal('Hello!');
    })

    it('Plural', function() {
      __(['%n message.', '%n messages.'], 1).should.equal('1 message.');
      __(['%n message.', '%n messages.'], 2).should.equal('2 messages.');
    })

    it('Placeholders (additional args)', function() {
      __('%s(1), %s(2) and %s(3).', 'ein', 'zwei', 'drei').should.equal('ein, zwei and drei.');
    })

    it('Placeholders (array)', function() {
      __('%s(1), %s(2) and %s(3).', [ 'ein', 'zwei', 'drei' ]).should.equal('ein, zwei and drei.');
    })

    it('Placeholders (object)', function() {
      __('%s(1), %s(2) and %s(3).', { '1': 'ein', '2': 'zwei', '3': 'drei' }).should.equal('ein, zwei and drei.');
    })

  })

  describe('Translation (known language):', function() {

    it('Switch language to Russian', function() {
      locale.lang = 'ru';
    })

    it('Simple', function() {
      __('Hello!').should.equal('Привет!');
    })

    it('Plural', function() {
      __(['%n message.', '%n messages.'], 1).should.equal('1 сообщение.');
      __(['%n message.', '%n messages.'], 2).should.equal('2 сообщения.');
      __(['%n message.', '%n messages.'], 5).should.equal('5 сообщений.');
    })

    it('Placeholders (additional args)', function() {
      __('%s(1), %s(2) and %s(3).', 'ein', 'zwei', 'drei').should.equal('ein, zwei и drei.');
    })

    it('Placeholders (array)', function() {
      __('%s(1), %s(2) and %s(3).', [ 'ein', 'zwei', 'drei' ]).should.equal('ein, zwei и drei.');
    })

    it('Placeholders (object)', function() {
      __('%s(1), %s(2) and %s(3).', { '1': 'ein', '2': 'zwei', '3': 'drei' }).should.equal('ein, zwei и drei.');
    })

  })

  describe('Translation (unknown language):', function() {

    it('Switch language to German', function() {
      locale.lang = 'de';
    })

    it('Simple', function() {
      __('Hello!').should.equal('Hello!');
    })

    it('Plural', function() {
      __(['%n message.', '%n messages.'], 1).should.equal('1 message.');
      __(['%n message.', '%n messages.'], 2).should.equal('2 messages.');
    })

  })

  describe('i18n objects:', function() {

    it('Create i18n object', function() {
      i18n = new locale.i18n();
    })

    it('Options inheritance', function() {
      i18n.lang.should.equal('de')
    })

    it('Constructor options', function(){
      i18n = new locale.i18n({ 'lang': 'ru' });
      i18n.lang.should.equal('ru');
    })

    it('Pull object methods to locals:', function() {
      __  = i18n.__;
    })

    it('Simple', function() {
      __('Hello!').should.equal('Привет!');
    })

    it('Plural', function() {
      __(['%n message.', '%n messages.'], 1).should.equal('1 сообщение.');
      __(['%n message.', '%n messages.'], 2).should.equal('2 сообщения.');
      __(['%n message.', '%n messages.'], 5).should.equal('5 сообщений.');
    })

    it('Switch object language on-the-fly (English):', function() {
      i18n.lang = 'en';
    })

    it('Simple', function() {
      __('Hello!').should.equal('Hello!');
    })

    it('Plural', function() {
      __(['%n message.', '%n messages.'], 1).should.equal('1 message.');
      __(['%n message.', '%n messages.'], 2).should.equal('2 messages.');
    })

  })

});