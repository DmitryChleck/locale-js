/*
 * # locale library
 *
 * ## locale.js: i18n for Node.js and browser
 * 
 * @author Dmitry A. Chleck <dmitrychleck@gmail.com>
 * @version 0.0.2
 */

module.exports = require('./lib/locale');
