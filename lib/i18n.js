/*
 * # locale library
 *
 * ## locale.js: i18n for Node.js and browser
 * 
 * @author Dmitry A. Chleck <dmitrychleck@gmail.com>
 * @version 0.0.2
 * @preserve
 */

/**
 * Translation rules for plural forms
 *
 * scheme =
 * {
 *   <lang>: [ <name>, <n>, <rule> ],
 *   ...
 * }
 * , where:
 *
 *   <lang> {String} Language key.
 *   Example: 'en' for English, 'ru' for Russin etc.
 *
 *   <name> {String} Language name.
 *   Example: 'English', 'Russian' etc.
 *
 *   <n> {Numeric} Number of plural forms for this language.
 *   Example: 2 for English, 3 for Russian.
 *
 *   <rule> {String} Javascript expression which takes n and returns right plural form number.
 *   Example: '(n == 1 ? 0 : 1)' for English.
 *
 * @api public
 */

var scheme = {
  "be": [ "Belarusian",         3, "(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)" ],
  "bg": [ "Bulgarian",          2, "" ],
  "cs": [ "Czech",              3, "(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2" ],
  "da": [ "Danish",             2, "" ],
  "de": [ "German",             2, "" ],
  "el": [ "Greek",              2, "" ],
  "en": [ "English",            2, "" ],
  "eo": [ "Esperanto",          2, "" ],
  "es": [ "Spanish",            2, "" ],
  "fa": [ "Persian",            1, "0" ],
  "fi": [ "Finnish",            2, "" ],
  "fr": [ "French",             2, "(n>1 ? 1 : 0)" ],
  "he": [ "Hebrew",             2, "" ],
  "hi": [ "Hindi",              2, "" ],
  "hy": [ "Armenian",           2, "" ],
  "hr": [ "Croatian",           3, "(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)" ],
  "hu": [ "Hungarian",          2, "" ],
  "id": [ "Indonesian",         1, "0" ],
  "is": [ "Icelandic",          2, "((n%10!=1 || n%100==11) ? 1 : 0)" ],
  "it": [ "Italian",            2, "" ],
  "ja": [ "Japanese",           1, "0" ],
  "ka": [ "Georgian",           1, "0" ],
  "kk": [ "Kazakh",             1, "0" ],
  "ko": [ "Korean",             1, "0" ],
  "ky": [ "Kyrgyz",             1, "0" ],
  "lt": [ "Lithuanian",         3, "(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2)" ],
  "lv": [ "Latvian",            3, "(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2)" ],
  "mn": [ "Mongolian",          2, "" ],
  "ms": [ "Malay",              1, "0" ],
  "nb": [ "Norwegian Bokmal",   2, "" ],
  "ne": [ "Nepali",             2, "" ],
  "nl": [ "Dutch",              2, "" ],
  "nn": [ "Norwegian Nynorsk",  2, "" ],
  "pl": [ "Polish",             3, "(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)" ],
  "pt": [ "Portuguese",         2, "" ],
  "ro": [ "Romanian",           3, "(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2)" ],
  "ru": [ "Russian",            3, "(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)" ],
  "sk": [ "Slovak",             3, "((n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2)" ],
  "sl": [ "Slovenian",          4, "(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0)" ],
  "so": [ "Somali",             2, "" ],
  "sq": [ "Albanian",           2, "" ],
  "sr": [ "Serbian",            3, "(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)" ],
  "sv": [ "Swedish",            2, "" ],
  "tg": [ "Tajik",              2, "(n>1 ? 1 : 0)" ],
  "th": [ "Thai",               1, "0" ],
  "tk": [ "Turkmen",            2, "" ],
  "tr": [ "Turkish",            2, "(n>1 ? 1 : 0)" ],
  "tt": [ "Tatar",              1, "0" ],
  "uk": [ "Ukrainian",          3, "(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)" ],
  "uz": [ "Uzbek",              2, "(n>1 ? 1 : 0)" ],
  "vi": [ "Vietnamese",         1, "0" ],
  "zh": [ "Chinese",            2, "(n>1 ? 1 : 0)" ]
};

var rules;

// Define namespace
var locale = new function() {
  // Save context
  var locale = this;

  /**
   * Translation dictionary
   *
   * tr =
   * {
   *   <lang>: {
   *     <key>: <translation>,
   *   },
   *   ...
   * }
   * , where:
   *
   *   <lang> {String} is language key.
   *   Example: 'en' for English, 'ru' for Russian etc.
   *
   *   <key>  {String} is translation key, the single or the first form of base language phrase.
   *   Example: 'Hello!' or 'There is %n object.'
   *
   *   <translation> {String}||{Array} is translation phrase(s), string for single or array of string for plural.
   *   Example: 'Привет!' or [ 'Имеется %n объект.', 'Имеется %n объекта.', 'Имеется %n объектов.']
   *
   * @api public
   */
  this.tr = {};

  function trimKey(s) {
    s = s.split('//');
    if(s.length < 2) return s[0];
    var tmp = s.pop();
    var comment = tmp.indexOf(' ');
    if(comment >= 0) tmp = tmp.substring(0, comment);
    if(tmp.length > 0 || s.length > 1) s.push(tmp);
    return s.join('//');
  }

  function trimPhrase(s) {
    s = s.split('//');
    if(s.length > 1) s.pop();
    return s.join('//');
  }

  /**
   * Fill placeholders in given string with args
   * fill(s, args)
   * fill(s, args, n)
   * @param {String} s String with placeholders. Placeholders are %n, %s and %s(name).
   * @param {Array} args %s and %s(name) will be replaced by args values in the same order, name will be ignored.
   * @param {Object} args %s will be replaced by '', %s(name) will be replaced by args value with the same name. 
   * @param undefined n %n will be replaced by ''.
   * @param {Number} n %n will be replaced by n value.
   * @return {String} Formatted string.
   * @api private
   */
  this.fill = function(s, args, n) {
    var argc = 1                // Current %s argument position
      , chunks = s.split('%')   // String parts
      , result = [ chunks[0] ]; // Result buffer
    // If args is array, convert to object
    if(Array.isArray(args)) {
      var tmp = {};
      // Shift args numeration to start from 1
      args.unshift('');
      for(var k in args) tmp[k] = args[k];
      args = tmp;
    }
    // Fill placeholders
    for(var i = 1; i < chunks.length; i++) {
      var chunk = chunks[i];
      var arg = '';
      switch(chunk[0]) {
        // %% => %
        case undefined:
          // Push '%' and next chunk
          result.push('%');
          result.push(chunks[++i]);
          continue;
        // %n => n
        case 'n':
          arg = n;
          break;
        // %s => value from args
        case 's':
          // Get name if present
          var name = '';
          // '((' after %s - ignore name
          if(chunk[1] === '(') {
            if(chunk[2] === '(') {
              // %s(( - %s with screened '('' after it but not %s(name)
              chunk = chunk.substring(1);
            } else {
              // %s(name)
              name = chunk.substr(2, chunk.indexOf(')') - 2);
              // Cut (name) at begin of chunk
              chunk = chunk.substring(name.length + 2);
            }
          }
          if(name) arg = args[name]; else arg = args[argc++];
          break;
      }
      result.push(arg);
      result.push(chunk.substring(1));
    }
    return result.join('');
  }

  /**
   * Translates given JSON args set.
   *
   * translate(args)
   * @param {JSON} args { '__i18n': {boolean}, 'phrase': <phrase>, 'n': {number}, 'args': {object} }
   * , where:
   * <phrase> is {string} for simple or array of {string} for plural.
   *
   * @return {string} Translated phrase.
   * @api private
   */
  this.translate = function(args) {
    var result, f;
    /*var args = arguments[0] || '';
    // Get arguments if direct call
    if(!args.__i18n) args = locale.args(arguments);*/
    if(!args.phrase) return '';
    // Return translation
    try {
      if(args.n === undefined) {
        // Phrase
        result = locale.tr[this.lang][args.phrase] || trimPhrase(args.phrase);
      } else {
        // Plural
        f = rules[this.lang].f(args.n);
        result = locale.tr[this.lang][args.phrase[0]][f];
      }
    } catch(e) {
      // Drop to base language if any error
      if(args.n === undefined) {
        // Base phrase
        result = trimPhrase(args.phrase);
      } else {
        try {
          // Base plural
          f = rules[locale.base].f(args.n);
          // Return right plural form or first form if no plural rule
          result = trimPhrase(args.phrase[f]);
        } catch(e) {
          result = trimPhrase(args.phrase[0]);
        }
      }
    }
    return locale.fill(result, args.args, args.n);
  }

  /**
   * Translates given phrase
   * 
   * __(<phrase>, <arg1>, <agr2>, ... ) Where phrase is {String} and arg1, arg2, ... is {String}.
   * __(<phrase>, <args>) Where phrase is {String} and args is {Array}.
   * __(<phrase>, <args>) Where phrase is {String} and args is {Object}.
   *
   * For plural form:
   * __([<form1>, <form2>, ... ], <number>, <arg1>, <agr2>, ... ) Where form1, form2, ... is {String}, number is {Numeric} and arg1, arg2, ... is {String}.
   * __([<form1>, <form2>, ... ], <number>, <args>) Where form1, form2, ... is {String}, number is {Numeric} and args is {Array}.
   * __([<form1>, <form2>, ... ], <number>, <args>) Where form1, form2, ... is {String}, number is {Numeric} and args is {Object}.
   *
   * @return {JSON} Arguments for translation in JSON format if this.remote.
   * @return {String} Translated phrase if not this.remote.
   * @api public
   */
  this.TR = function() {
    var phrase, n;
    // Convert arguments to array
    var a = Array.prototype.slice.call(arguments);
    // Get phrase
    phrase = a.shift() || '';
    // Get n if plural
    if(Array.isArray(phrase)) n = a.shift();
    // Trim comments but save id
    if(n === undefined) {
      phrase = trimKey(phrase);
    } else {
      for(var i in phrase) {
        phrase[i] = trimKey(phrase[i]);
      }
    }
    // Pop array and object arguments
    if(typeof a[0] === 'object' || Array.isArray(a[0])) {
      a = a[0];
    }
    var json = { '__i18n': true, 'phrase': phrase, 'n': n, 'args': a };
    // Check remote mode
    return this.remote ? json : locale.translate.call(this, json);
  }

  /**
   * Remote translation client
   * Searches for __i18n marked objects in given JSON and translates it.
   *
   * @param {JSON} json JSON object for translation.
   * @return {JSON} Translated object.
   * @api public
   */
  this.JSON = function(json) {
    if(typeof json !== 'object') return;
    for(var o in json) {
      if(json[o].__i18n) json[o] = locale.translate.call(this, json[o]); else locale.JSON(json[o]);
    }
  }

  /**
   * Proxy function which uses a global (library) context, see TR()
   * @api public
   */
  this.__ = function() { return locale.TR.apply(locale, arguments); }

  /**
   * Proxy function which uses a global (library) context, see JSON()
   * @api public
   */
  this.__JSON = function() { return locale.JSON.apply(locale, arguments); }

  /**
   * i18n objects constructor
   * @param {Object} options:
   *   lang: translation language (default is current global lang)
   * @return {i18n} object
   * @api public
   */
  this.i18n = function(options)
  {
    var self = this;
    options = options || {};
    this.lang = options.lang || locale.lang;
  
    /**
     * Proxy function which uses a local (object) context, see global TR()
     * @api public
     */
    this.__ = function() { return locale.TR.apply(self, arguments); }

    /**
     * Proxy function which uses a local (object) context, see global JSON()
     * @api public
     */
    this.__JSON = function() { return locale.JSON.apply(locale, arguments); }
  }

  // Compile rules functions
  rules = {};
  for(var lang in scheme) {
    rules[lang] = scheme[lang];
    // Create rule function (Use default rule if not exists)
    rules[lang]['f'] = Function('n', 'return ' + (rules[lang][2] || '(n == 1 ? 0 : 1)'));
  }
  this.rules = rules;

} // End of locale namespace

// export locale if node.js
if(typeof exports !== 'undefined') exports.locale = locale;
