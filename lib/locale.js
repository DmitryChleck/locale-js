var locale = require('./i18n').locale;

/*
 * # node.js layer
 *
 * ## locale.js: i18n for Node.js and browser
 * 
 * @author Dmitry A. Chleck <dmitrychleck@gmail.com>
 * @version 0.0.2
 */

var fs    = require('fs')
  , path  = require('path');

/**
 * Load JSON from file.
 * @param {String} file File to load.
 * @param {Bool} useLibraryPath Load path to this file + file if true or options.path + file in other case.
 * @return {Object} JSON object or undefined if can't load.
 * @api private
 */
function loadJson(file, useLibraryPath) {
  try {
    var p = useLibraryPath ? path.dirname(__filename) : locale.path;
    return JSON.parse(fs.readFileSync(path.join(p, file + '.json')));
  } catch(e) {}
}

/**
 * Library initialization.
 * init()
 * @api public
 */
locale.init = function(options) {
  options = options || {};
  // Path to locale folder
  locale.path = options.path || '';
  // Try to load config
  var conf = loadJson('i18n') || {};
  // Base language of translation (translate from)
  locale.base = options.base || conf.base || 'en';
  // Global translation language (translate to)
  locale.lang = options.lang || conf.lang || locale.base;
  // Load translations
  for(var lang in locale.rules) {
    // Try to load translation for each known language
    var tmp = loadJson(lang);
    // Add translation if success
    if(tmp) locale.tr[lang] = tmp;
  }
}

module.exports = locale;
