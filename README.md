# Locale for Javascript

# Features
---
* multiple language (translate to)
* multiple base language (translate from)
* plural forms for any language
* flexible placeholders
* simultaneous support for multiple languages

## It's simple
---
    // Translate
    __('Hello!');

    // Translate plural
    __([
      'There is %n item in your cart.',
      'There are %n items in your cart.'
      ], cart.items.length
    );

## Placeholders support
---
    // Phrase with placeholders
    __('%s is the new wonderful library for %s.', 'locale', 'javascript');

    // Plural with placeholders
    __([
      'There is %n %s library for %s.',
      'There are %n %s libraries for %s.'
      ], n, 'good', 'javascript'
    );

## Placeholders args variants
---
You can pass placeholder's args as additional args for translate function:

    // Additional args
    __('%s(lib) is the new wonderful library for %s(lang).', 'locale', 'javascript');

As array:

    // Array args
    __('%s(lib) is the new wonderful library for %s(lang).', [ 'locale', 'javascript' ]);

Or as object

    // Object args
    __('%s(lib) is the new wonderful library for %s(lang).',
      {
        'lib': 'locale',
        'lang':'javascript'
      }
    );

## Placeholders rules
---
For additional args and array args placeholders will be replaced by:

* %s -> the same order arg
* %s(<key>) -> same as %s, <name> will be ignored

For object args:

* %s -> ''
* %s(<key>) -> args[<key>] value

Also %n placeholder is available for plural form:

* %n -> value of number parameter for plural

## Translation language selection
---
You can set translation language in library init() call:

    locale.init({'lang': 'ru'});

And change it at any time when you need:

    locale.lang = 'ru';

# API
---

## Library import
---

For node.js:

    var locale = require('locale-js');

For browser:

    <script type="text/javascript" src="i18n.js"></script>

## Translation
---

For simple form:

**__(phrase)**
**__(phrase, ...)**
**__(phrase, array)**
**__(phrase, object)**

, where:

- *phrase (string) - string for translation (can contains placeholders);
- *...* (mixed) - any number of additional args;
- *array* (Array) - array of args;
- *object* (object) - map of args.

### Example:

    // Hello!
    __('Hello!');
    // Hello, anonymous!
    __('Hello, %s(1)!', 'anonymous');
    // Hello, John Smith!
    __('Hello, %s(1) %s(2)!', [ 'John', 'Smith' ]);
    // Hello, Foo (bar)!
    __('Hello, %s(name) (%s(login))!', { name: 'Foo', login: 'bar' });

For plural form:

**__(phrase, n)**
**__(phrase, n, ...)**
**__(phrase, n, array)**
**__(phrase, n, object)**

, where:

- *phrase (array) - array of strings each of them is a plural form for translation, can contains placeholders;
- *n* (numeric) - base number of plural form;
- *...* (mixed) - any number of additional args;
- *array* (Array) - array of args;
- *object* (object) - map of args.

### Example:

    // Inbox: 1 unreaded message.
    __([ 'Inbox: %n unreaded message.', 'Inbox: %n unreaded messages.' ], 1);
    // Inbox: 7 unreaded messages.
    __([ '%s(1): %n unreaded message.', '%s(1): %n unreaded messages.' ], 7, 'Inbox');
    // Anonymous, you have 365 unreaded messages in "Spam" folder.
    __([
        '%s(1), you have %n unreaded message in "%s(2)" folder.',
        '%s(1), you have %n unreaded messages in "%s(2)" folder.'
       ], 365, [ 'Anonymous', 'Spam' ]);
    // The second way
    __([
        '%s(login), you have %n unreaded message in "%s(2)" folder.',
        '%s(folder), you have %n unreaded messages in "%s(2)" folder.'
       ], 365, [ login: 'Anonymous', folder: 'Spam' ]);
